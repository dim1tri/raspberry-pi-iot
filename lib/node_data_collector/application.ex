defmodule NodeDataCollector.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false
  require Logger

  use Application

  def start(_type, _args) do
    opts = [strategy: :one_for_one, name: NodeDataCollector.Supervisor, max_restarts: 100]

    Logger.info("Loooggeeer before sleep")
    IO.puts("IOoooo before sleep")
    Process.sleep(60 * 1000)
    Logger.info("Looooggeeeer after sleep")
    IO.puts("IOoooo after sleep")

    children =
      [
        # Marina Vela
        # %{
        # id: :freezer_connector_BDF2,
        # start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_BDF2"]]}
        # },
        # %{
        # id: :freezer_connector_BDF6,
        # start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_BDF6"]]}
        # },
        ## Not working
        ## %{
        ## id: :freezer_connector_BDF7,
        ## start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_BDF7"]]}
        ## },
        # %{
        # id: :freezer_connector_4266,
        # start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_4266"]]}
        # },
        # %{
        # id: :freezer_connector_4273,
        # start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_4273"]]}
        # },
        # %{
        # id: :oven_connector_4273,
        # start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_4274"]]}
        # },
        # %{
        # id: :oven_connector_BC91,
        # start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_BC91"]]}
        # },
        # %{
        # id: :oven_connector_BC93,
        # start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_BC93"]]}
        # },
        # %{
        # id: :oven_connector_BC96,
        # start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_BC96"]]}
        # },
        # Bruc
        %{
          id: :freezer_connector_85BD,
          start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_85BD"]]}
        },
        %{
          id: :freezer_connector_85AD,
          start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_85AD"]]}
        },
        %{
          id: :freezer_connector_6CC4,
          start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_6CC4"]]}
        },
        %{
          id: :freezer_connector_85B0,
          start: {NodeDataCollector.FreezerConnector, :start_link, [[hostname: "Freezer_85B0"]]}
        },
        %{
          id: :oven_connector_BC8D,
          start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_BC8D"]]}
        },
        %{
          id: :oven_connector_BC8E,
          start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_BC8E"]]}
        },
        %{
          id: :oven_connector_426E,
          start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_426E"]]}
        },
        %{
          id: :oven_connector_BDFA,
          start: {NodeDataCollector.OvenConnector, :start_link, [[hostname: "Oven_BDFA"]]}
        },
        {Tortoise.Connection,
         [
           client_id: "RaspberryPiBruc",
           # client_id: "RaspberryPi",
           server: {
             Tortoise.Transport.SSL,
             certfile:
               IO.inspect(
                 Path.join(:code.priv_dir(:node_data_collector), "d56e062da6-certificate.pem.crt")
                 # Path.join(:code.priv_dir(:node_data_collector), "30afc0f08c-certificate.pem.crt")
               ),
             keyfile:
               IO.inspect(
                 Path.join(:code.priv_dir(:node_data_collector), "d56e062da6-private.pem.key")
                 # Path.join(:code.priv_dir(:node_data_collector), "30afc0f08c-private.pem.key")
               ),
             host: 'a13uuqxnb95b6q-ats.iot.eu-central-1.amazonaws.com',
             port: 8883,
             cacerts: :certifi.cacerts(),
             versions: [:"tlsv1.2"],
             customize_hostname_check: [
               match_fun: :public_key.pkix_verify_hostname_match_fun(:https)
             ]
           },
           handler: {Tortoise.Handler.Logger, []}
         ]}
        # Children for all targets
        # Starts a worker by calling: NodeDataCollector.Worker.start_link(arg)
        # {NodeDataCollector.Worker, arg},
      ] ++ children(target())

    Supervisor.start_link(children, opts)
  end

  # List all child processes to be supervised
  def children(:host) do
    [
      # Children that only run on the host
      # Starts a worker by calling: NodeDataCollector.Worker.start_link(arg)
      # {NodeDataCollector.Worker, arg},
    ]
  end

  def children(_target) do
    [
      # Children for all targets except host
      # Starts a worker by calling: NodeDataCollector.Worker.start_link(arg)
      # {NodeDataCollector.Worker, arg},
    ]
  end

  def target() do
    Application.get_env(:node_data_collector, :target)
  end
end
