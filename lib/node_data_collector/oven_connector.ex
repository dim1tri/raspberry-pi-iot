defmodule NodeDataCollector.OvenConnector do
  use GenServer
  require Logger

  @port 23
  @thing "RaspberryPiBruc"
  # @thing "RaspberryPi"
  @topic "oven/status"

  def send_message(pid, message) do
    GenServer.cast(pid, {:message, message})
  end

  def start_link(state) do
    GenServer.start_link(
      __MODULE__,
      %{
        socket: nil,
        hostname: state[:hostname],
        door: %{
          state: nil
        }
      }
    )
  end

  def init(state) do
    send(self(), :connect)
    {:ok, state}
  end

  def handle_info(:connect, state) do
    Logger.info("Connecting to #{state[:hostname]}")

    case :gen_tcp.connect(to_charlist(state[:hostname]), @port, [
           :binary,
           active: true,
           packet: :line
         ]) do
      {:ok, socket} ->
        Logger.info("Connected to #{state[:hostname]}")
        # Process.send_after(self(), :read_temperatures, 10000)
        {:noreply, %{state | socket: socket}}

      {:error, reason} ->
        disconnect(state, reason)
    end
  end

  def handle_info({:tcp, _, <<"Opening Door", _::bitstring>> = msg}, state)
      when is_bitstring(msg) do
    aws_msg = Enum.join(for <<c::utf8 <- msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: #{aws_msg}")
    {:noreply, put_in(state, [:door, :state], "opening"), {:continue, :update_aws_iot_shadow}}
  end

  def handle_info({:tcp, _, <<">Door: OPEN", _::bitstring>> = msg}, state)
      when is_bitstring(msg) do
    aws_msg = Enum.join(for <<c::utf8 <- msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: #{aws_msg}")
    {:noreply, put_in(state, [:door, :state], "open"), {:continue, :update_aws_iot_shadow}}
  end

  def handle_info({:tcp, _, <<"Closing Door", _::bitstring>> = msg}, state)
      when is_bitstring(msg) do
    aws_msg = Enum.join(for <<c::utf8 <- msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: #{aws_msg}")
    {:noreply, put_in(state, [:door, :state], "closing"), {:continue, :update_aws_iot_shadow}}
  end

  def handle_info({:tcp, _, <<">Door: CLOSE", _::bitstring>> = msg}, state)
      when is_bitstring(msg) do
    aws_msg = Enum.join(for <<c::utf8 <- msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: #{aws_msg}")
    {:noreply, put_in(state, [:door, :state], "closed"), {:continue, :update_aws_iot_shadow}}
  end

  def handle_info({:tcp, _, msg}, state) do
    aws_msg = Enum.join(for <<c::utf8 <- msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: #{aws_msg}")
    {:noreply, state}
  end

  def handle_info({:tcp_closed, m}, state) do
    IO.inspect(m)
    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, m}, state) do
    IO.inspect(m)
    {:stop, :normal, state}
  end

  def handle_info(unknown_msg, state) do
    aws_msg = Enum.join(for <<c::utf8 <- unknown_msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: #{aws_msg}")
    IO.inspect(unknown_msg)
    {:noreply, state}
  end

  def handle_cast({:message, message}, %{socket: socket} = state) do
    :ok = :gen_tcp.send(socket, message)
    {:noreply, state}
  end

  def handle_cast(unknown_msg, state) do
    aws_msg = Enum.join(for <<c::utf8 <- unknown_msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: Failed to parse: #{aws_msg}")
    IO.inspect(unknown_msg)
    {:noreply, state}
  end

  def handle_call(unknown_msg, _, state) do
    aws_msg = Enum.join(for <<c::utf8 <- unknown_msg>>, do: <<c::utf8>>)
    Logger.info("#{state[:hostname]}: Failed to parse: #{aws_msg}")
    IO.inspect(unknown_msg)
    {:reply, :ok}
  end

  def handle_continue(:update_aws_iot_shadow, state) do
    {:ok, payload} =
      Jason.encode(%{
        id: state[:hostname],
        door: state[:door]
      })

    Logger.info("#{state[:hostname]}: #{payload}")
    # Tortoise.publish(@thing, @topic, payload, qos: 0)

    {:noreply, state}
  end

  def disconnect(state, reason) do
    Logger.info("#{state[:hostname]} disconnected: #{reason}")
    IO.puts("Disconnected: #{reason}")

    {:stop, :normal, state}
  end
end
