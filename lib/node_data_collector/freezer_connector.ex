defmodule NodeDataCollector.FreezerConnector do
  use GenServer
  require Logger

  @port 23
  @thing "RaspberryPiBruc"
  # @thing "RaspberryPi"
  @topic "freezer/status"

  def send_message(pid, message) do
    GenServer.cast(pid, {:message, message})
  end

  def start_link(state) do
    GenServer.start_link(
      __MODULE__,
      %{
        socket: nil,
        hostname: state[:hostname],
        top_drawer: %{
          state: nil,
          temperature: nil
        },
        bottom_drawer: %{
          state: nil,
          temperature: nil
        }
      }
    )
  end

  def init(state) do
    send(self(), :connect)
    {:ok, state}
  end

  def handle_info(:connect, state) do
    Logger.info("Connecting to #{state[:hostname]}")

    case :gen_tcp.connect(to_charlist(state[:hostname]), @port, [
           :binary,
           active: true,
           packet: :line
         ]) do
      {:ok, socket} ->
        Logger.info("Connected to #{state[:hostname]}")
        Process.send_after(self(), :read_temperatures, 60000)
        {:noreply, %{state | socket: socket}}

      {:error, reason} ->
        disconnect(state, reason)
    end
  end

  def handle_info({:tcp, _, <<"readt", _::bitstring>> = msg}, state) when is_bitstring(msg) do
    {:noreply, state}
  end

  def handle_info({:tcp, _, <<">readt", _::bitstring>> = msg}, state) when is_bitstring(msg) do
    {:noreply, state}
  end

  def handle_info(
        {:tcp, _, <<"Temp Drawer ", drawer_id_bin::binary-size(1), ": ", rest::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)
    {temperature, _} = Float.parse(rest)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :temperature], temperature),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :temperature], temperature),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info(
        {:tcp, _, <<">Opening Drawer ", drawer_id_bin::binary-size(1), _::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :state], "opening"),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :state], "opening"),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info(
        {:tcp, _, <<">Closing Drawer ", drawer_id_bin::binary-size(1), _::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :state], "closing"),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :state], "closing"),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info(
        {:tcp, _, <<">Drawer ", drawer_id_bin::binary-size(1), ": OPEN", _::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :state], "open"),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :state], "open"),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info(
        {:tcp, _,
         <<"Drawer ", drawer_id_bin::binary-size(1), " is already open", _::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :state], "open"),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :state], "open"),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info(
        {:tcp, _,
         <<"Drawer ", drawer_id_bin::binary-size(1), " is already close", _::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :state], "closed"),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :state], "closed"),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info(
        {:tcp, _, <<">Drawer ", drawer_id_bin::binary-size(1), ": CLOSE", _::bitstring>> = msg},
        state
      )
      when is_bitstring(msg) do
    {drawer_id, _} = Integer.parse(drawer_id_bin)

    case drawer_id do
      1 ->
        {:noreply, put_in(state, [:top_drawer, :state], "closed"),
         {:continue, :update_aws_iot_shadow}}

      2 ->
        {:noreply, put_in(state, [:bottom_drawer, :state], "closed"),
         {:continue, :update_aws_iot_shadow}}
    end
  end

  def handle_info({:tcp, _, ">"}, state) do
    {:noreply, state}
  end

  def handle_info({:tcp, _, msg}, state) do
    Logger.info("#{state[:hostname]}: Failed parsing: #{msg}")
    {:noreply, state}
  end

  def handle_info({:tcp_closed, m}, state) do
    IO.inspect(m)
    {:stop, :normal, state}
  end

  def handle_info({:tcp_error, m}, state) do
    IO.inspect(m)
    {:stop, :normal, state}
  end

  def handle_info(:read_temperatures, state) do
    IO.puts("Attempting reading temperature")
    GenServer.cast(self(), {:message, "readt\n"})
    Process.send_after(self(), :read_temperatures, 10000)
    {:noreply, state}
  end

  def handle_info(unknown_msg, state) do
    IO.puts("Unknown message")
    Logger.info("Unknown message")
    IO.inspect(unknown_msg)
    {:noreply, state}
  end

  def handle_cast({:message, message}, %{socket: socket} = state) do
    :ok = :gen_tcp.send(socket, message)
    {:noreply, state}
  end

  def handle_cast(unknown_msg, state) do
    Logger.info("Unknown message from #{state[:hostname]}")
    Logger.info(unknown_msg)
    IO.inspect(unknown_msg)
    {:noreply, state}
  end

  def handle_call(unknown_msg, _, state) do
    Logger.info("Unknown message from #{state[:hostname]}")
    Logger.info(unknown_msg)
    IO.inspect(unknown_msg)
    {:reply, :ok}
  end

  def handle_continue(:update_aws_iot_shadow, state) do
    {:ok, payload} =
      Jason.encode(%{
        id: state[:hostname],
        top_drawer: state[:top_drawer],
        bottom_drawer: state[:bottom_drawer]
      })

    Logger.info("#{state[:hostname]}: #{payload}")

    Tortoise.publish(@thing, @topic, payload, qos: 0)

    {:noreply, state}
  end

  def disconnect(state, reason) do
    Logger.info("Disconnected: #{reason}")
    IO.puts("Disconnected: #{reason}")
    :timer.sleep(1000)

    {:stop, :normal, state}
  end
end
