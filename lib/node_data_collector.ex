defmodule NodeDataCollector do
  @moduledoc """
  Documentation for NodeDataCollector.
  """

  @doc """
  Hello world.

  ## Examples

      iex> NodeDataCollector.hello
      :world

  """
  def hello do
    :world
  end
end
