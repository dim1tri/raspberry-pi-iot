# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
import Config

# Enable the Nerves integration with Mix
Application.start(:nerves_bootstrap)

config :node_data_collector, target: Mix.target()

# Customize non-Elixir parts of the firmware. See
# https://hexdocs.pm/nerves/advanced-configuration.html for details.

config :nerves, :firmware, rootfs_overlay: "rootfs_overlay"

# Set the SOURCE_DATE_EPOCH date for reproducible builds.
# See https://reproducible-builds.org/docs/source-date-epoch/ for more information

config :nerves, source_date_epoch: "1617787055"

# Use Ringlogger as the logger backend and remove :console.
# See https://hexdocs.pm/ring_logger/readme.html for more information on
# configuring ring_logger.

config :logger,
  backends: [RingLogger, CloudWatch],
  utc_log: true

config :logger, CloudWatch,
  access_key_id: "",
  secret_access_key: "",
  region: "eu-central-1",
  # endpoint: "amazonaws.com",
  # metadata: [:m1, :m2],
  # metadata_filter: [module: MyModule]
  log_group_name: "raspberry-pi-iot",
  log_stream_name: "bruc",
  max_buffer_size: 10_485,
  max_timeout: 60_000

if Mix.target() == :host or Mix.target() == :"" do
  import_config "host.exs"
else
  import_config "target.exs"
end
